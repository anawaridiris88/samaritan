import os
import logging

from binance_chain.http import HttpApiClient
from binance_chain.wallet import Wallet
from binance_chain.messages import TransferMsg, Transfer
from binance_chain.environment import BinanceEnvironment
from binance_chain.exceptions import BinanceChainAPIException

from common import HttpClient, Coin


class Binance(HttpClient):
    """
    A wrapper for binance chain interactions
    """

    chain = "BNB"

    def __init__(self):
        env = BinanceEnvironment.get_production_env()
        self.client = HttpApiClient(env=env)
        self.wallet = Wallet.create_wallet_from_mnemonic(
            os.environ.get("MNEMONIC"), env=env
        )

    def balance(self, addr=None):
        if addr is None:
            addr = self.get_address()
        account = self.client.get_account(addr)
        coins = []
        for b in account["balances"]:
            coins.append(Coin(b["symbol"], float(b["free"]) * 1e8))
        return coins

    def get_address(self):
        return self.wallet.address

    def get_transaction(self, hash_id):
        resp = self.client.get_transaction(hash_id)
        result = resp["tx"]["value"]["msg"][0]["value"]
        result['memo'] = resp["tx"]["value"]["memo"]
        return result

    def transfer(self, from_addr, to_addr, denom, amount, memo=""):
        transfer_msg = TransferMsg(
            wallet=self.wallet,
            symbol=denom,
            amount=amount / 1e8,
            to_address=to_addr,
            memo=memo,
        )
        self.wallet.reload_account_sequence()
        try:
            resp = self.client.broadcast_msg(transfer_msg, sync=True)
        except BinanceChainAPIException:
            logging.exception("failed to broadcast msg")
        return resp

    def stake(self, from_addr, to_addr, denom, rune, asset):
        multi_transfer_msg = TransferMsg(
            wallet=self.wallet,
            transfers=[
                Transfer(symbol=denom, amount=asset),
                Transfer(symbol="RUNE_B1A", amount=rune),
            ],
            to_address=to_addr,
            memo="STAKE:BNB." + denom,
        )

        resp = self.client.broadcast_msg(multi_transfer_msg, sync=True)
        self.wallet.reload_account_sequence()
        return resp


class MockBinance(HttpClient):
    """
    A local simple implementation of mock binance chain
    """

    chain = "BNB"

    def __init__(self, url="http://host.docker.internal:26660"):
        self.base_url = url

    def balance(self, addr):
        coins = []
        data = self.fetch("/account/" + addr)
        for b in data["balances"]:
            coins.append(Coin(b["denom"], b["amount"]))
        return coins

    def transfer(self, from_addr, to_addr, denom, amount, memo=""):
        return self.post(
            "/broadcast/easy",
            [
                {
                    "from": from_addr,
                    "to": to_addr,
                    "coins": [{"denom": denom, "amount": amount}],
                    "memo": memo,
                }
            ],
        )

    def stake(self, from_addr, to_addr, denom, rune, asset):
        return self.post(
            "/broadcast/easy",
            [
                {
                    "from": from_addr,
                    "to": to_addr,
                    "coins": [
                        {"denom": denom, "amount": asset},
                        {"denom": "RUNE-B1A", "amount": rune},
                    ],
                    "memo": "STAKE:BNB." + denom,
                }
            ],
        )

    def seed(self, to_addr, denom, amount):
        return self.transfer(
            "tbnb1ht7v08hv2lhtmk8y7szl2hjexqryc3hcldlztl", to_addr, denom, amount
        )
